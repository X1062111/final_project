var user_email = '';
var user_name=""; 
var user_photoURL="";
var loginUserid="";
var otheruserid="";
//初始化
window.onload = function () {
    init();
    initdropdown();
};
function init() {
    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            notice_permission();
            loginUserid = firebase.auth().currentUser.uid.toString();
            var loginUseridpublic = firebase.database().ref('users/'+ loginUserid + '/public' );
            loginUseridpublic.transaction(function(currentData) {
                if (currentData === null) {
                    if(user.photoURL) return{
                        'email': user.email,
                        'name': user.displayName,
                        'photoURL': user.photoURL
                    }
                    else if(user.displayName) return {
                        'email': user.email,
                        'name': user.displayName,
                        'photoURL': 'https://image.flaticon.com/icons/svg/37/37943.svg'
                    }
                    else return {
                        'email': user.email,
                        'name': user.email.split('@')[0],
                        'photoURL': 'https://image.flaticon.com/icons/svg/37/37943.svg'
                    }
                }
            }, function(error, committed, snapshot) {
                firebase.database().ref('users/'+ loginUserid + '/public').on('value',function(data){
                    user_email = data.val().email;
                    user_name = data.val().name;
                    user_photoURL = data.val().photoURL;
                    menu.innerHTML = "<a href='#' id='setting-btn' onclick='changeotheruserid("+'"'+loginUserid+'"'+")'>"
                     + user_name + "</a>"+"<a href='signin.html' id='logout-btn'>Logout</a>";

                     //settingbtn
                     var btnsetting = document.getElementById('setting-btn');
                     btnsetting.addEventListener('click', setting );
                     //logoutbtn
                     var btnlogout = document.getElementById('logout-btn');
                     btnlogout.addEventListener('click', function(){firebase.auth().signOut();})

                     firebase.database().ref('userlist/'+loginUserid).transaction(function(currentData) {
                         if (currentData === null) return {'name': data.val().name}
                     })
                })
                otheruserid = loginUserid;
                setfriend();
                showmessagesto();
                checknewmessage();
            });
        } else {
            // It won't show any post if not login
            window.location.href="signin.html";
            menu.innerHTML = "<a href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
}
function initdropdown(){
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;
    
    for (i = 0; i < dropdown.length; i++) {
      dropdown[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
          dropdownContent.style.display = "none";
        } else {
          dropdownContent.style.display = "block";
        }
      });
    }
}
//提醒
function notice_permission(){
    if (!Notification) { alert('Desktop notifications not available in your browser. Try Chromium.'); return; }
    if (Notification.permission !== "granted")
        Notification.requestPermission();
}
function notifyMe(fromwho,text) {
    if (Notification.permission !== "granted")
      Notification.requestPermission();
    else {
        if( fromwho != loginUserid && fromwho != otheruserid ){
            firebase.database().ref('users/' + fromwho + '/public').once('value').then(function(snapshot){
                var fromwho_name = snapshot.val().name;
                var notification = new Notification('Notification', {
                icon:  snapshot.val().photoURL,
                body: fromwho_name+" : "+text,
                });
            
                notification.onclick = function () {
                window.open("signin.html");      
                };
            })
        }
    }
}
//確認是否有新訊息以提醒
function checknewmessage(){
    var messageRef = firebase.database().ref("message/"+loginUserid);
    firebase.database().ref('userlist').once('value').then(function(snapshot){
        snapshot.forEach(function(childSnapshot) {
            var counter = 0;
            var length;
            messageRef.child(childSnapshot.key).once('value').then(function(parentdata){
                length = parentdata.numChildren();
                messageRef.child(childSnapshot.key).on('child_added',function(data){
                    counter+=1;
                    if(counter>length){
                        notifyMe(data.val().user_id,data.val().text);
                    }
                })
            })
        });

    })
}

//設定畫面
function setting() {
    otheruserid = loginUserid;
    document.getElementById('chat').innerHTML =
    '<div class="container">'+
        '<div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">'+
            '<img class="mr-3" src="https://getbootstrap.com/assets/brand/bootstrap-outline.svg" alt="" width="48" height="48">'+
            '<div class="lh-100">'+
                '<h6 class="mb-0 text-white lh-100">Midterm Project</h6>'+
                '<small>Software Studio</small>'+
            '</div>'+
        '</div>'+
        '<div id="setting">'+
            '<div class="text-center" style="font-size:15px">'+
                '<img id="showimg" src="'+user_photoURL+'" class="img-thumbnail"><br>'+
                '更改圖片: '+
                '<input id="upload" class="btn btn-danger" type="file" accept="image/gif, image/jpeg, image/png" onchange="readimg(this.files)"><br>'+
                '<p class="setting-form">'+
                    '<a>Change name</a>'+
                    '<input type="text" id="changename" class="form-control" placeholder="'+user_name+'" required>'+
                    '<button class="btn btn-lg btn-primary btn-block" onclick="changename()">sent</button>'+
                '</p>'+
            '</div>'+
        '</div>'+
    '</div>';
}
//訊息格
function showmessagesto(){
    var str_before_otherusername = "<a href='#Bot' id='";
    var str_after_id = "' onClick='changeotheruserid(this.id)'>";
    var str_after_otherusername = "</a>";
    var namesRef = firebase.database().ref('users/' + loginUserid + '/touser');
    var total_otheruserid = [];
    var zero_count = 0;
    var first_count = 0;
    var second_count = 0;
    namesRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot) {
                var childdata;
                if(childSnapshot.val().isfriend==true){
                    zero_count+=1;
                    var otherdata=firebase.database().ref('users/' + childSnapshot.key + '/public');
                    otherdata.once('value').then(function (snapshot) {
    
                        childdata = snapshot.child("name").val();
                        total_otheruserid[total_otheruserid.length] = str_before_otherusername 
                        + childSnapshot.key + str_after_id + childdata  + str_after_otherusername;
                        first_count+=1;
                        
                        otherdata.on('value',function(newsnapshot){
                            document.getElementById(childSnapshot.key).innerHTML = newsnapshot.child("name").val();
                        })
    
                        if(zero_count==first_count) {//當資料都載好的時候再做以下的事
                            setmessagesto(total_otheruserid.join(''));
    
                            namesRef.on('child_added',function(data){
                                second_count+=1;//已呈現的內容不再取
                                if(second_count>first_count){
                                    var childdata;
                                    var othernewdata=firebase.database().ref('users/' + data.key + '/public');
                                    othernewdata.once('value').then(function (snapshot) {
                                        childdata = snapshot.child("name").val();
                                        total_otheruserid[total_otheruserid.length] = str_before_otherusername 
                                        + data.key + str_after_id + childdata  + str_after_otherusername;
                                        othernewdata.on('value',function(newsnapshot){
                                            document.getElementById(data.key).innerHTML = newsnapshot.child("name").val();
                                        })
                                        setmessagesto(total_otheruserid.join(''));
                                    });
                                }
                            })
    
                        }
                    });
                }
            });
        })
        .catch(e => console.log(e.message));
}
function setmessagesto(str){
    document.getElementById('message-menu').innerHTML = str;
}
//換對象
function changeotheruserid(id){
    otheruserid=id;
    var f = ( document.body.clientHeight>navbarheight )? 17 : 0; //17=卷軸寬度
    if( document.body.clientWidth+f<=767.98 ){
        document.getElementsByClassName('offcanvas-collapse')[0].className='offcanvas-collapse';
        changemain();
    }
    if( loginUserid!=otheruserid ) showcontent();
}
//對話畫面
function showcontent(){
    document.getElementById('chat').innerHTML =
    '<div class="container">'+
        '<div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">'+
            '<img class="mr-3" src="https://getbootstrap.com/assets/brand/bootstrap-outline.svg" alt="" width="48" height="48">'+
            '<div class="lh-100">'+
                '<h6 class="mb-0 text-white lh-100">Midterm Project</h6>'+
                '<small>Software Studio</small>'+
            '</div>'+
        '</div>'+
        '<div id="post_list">'+
        '</div>'+
        '<div class="my-3 p-3 bg-white rounded box-shadow">'+
            '<h5 class="border-bottom border-gray pb-2 mb-0">Message</h5>'+
            '<textarea class="form-control" rows="3" id="comment" style="resize:none"></textarea>'+
            '<div class="media text-muted pt-3"> '+
                '<button id="post_btn" type="button" class="btn btn-success" onclick="sendmessage()">'+
                    'Submit'+
                '</button>'+
            '</div>'+
        '</div>'+
    '</div>';
    // The html code for post
    var str_before_id = "<div class='my-3 p-3 bg-white rounded box-shadow' id='"
    var str_after_id = "'>";
    var str_before_photo = "<div class='media pt-3'><div class='media-left'><img src='";
    var str_before_username = "' alt='' class='mr-2 rounded' style='height:60px; width:60px;'></div>"+
                              "<div class='media-body'><div class='border-bottom border-gray pb-0 mb-0'><h6>";
    var str_before_content = "</h6></div><p class='pb-1 mb-0 small lh-125 border-bottom border-gray'>"
    var str_before_time = "<small class='d-block text-gray-dark' style='color:gray; font-size:2px; text-align: right'>"
    var str_after_content = "</small></p></div></div></div>\n";
    
    var postsRef = firebase.database().ref("message/"+loginUserid+"/"+otheruserid);
    var otherdata = firebase.database().ref('users/' + otheruserid + '/public');
    var total_post = [];
    var zero_count = 0;
    var first_count = 0;
    var second_count = 0;

    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function(childSnapshot) {
                zero_count+=1;
                var childdata = childSnapshot.val();
                var a = str_before_id + childSnapshot.key + str_after_id + str_before_photo;
                var b = str_before_content + childdata.text.split('\n').join('<br>') + str_before_time 
                        + totime(childdata.createdAt,8)  + str_after_content;
                otherdata.once('value').then(function (othersnapshot) {
                    total_post[total_post.length] = ( otheruserid == childdata.user_id ) ? 
                    a + othersnapshot.val().photoURL + str_before_username + othersnapshot.val().name + b :
                    a + user_photoURL + str_before_username + user_name + b ;
                    first_count+=1;

                    if(zero_count==first_count){
                        document.getElementById('post_list').innerHTML = total_post.join(' ');
                        snapshot.forEach(function(childSnapshot2) {
                            var childdata2 = childSnapshot2.val();
                            var c = str_before_content + childdata2.text.split('\n').join('<br>') + str_before_time 
                                    + totime(childdata2.createdAt,8)  + "</small></p></div></div>";
                            otherdata.on('value',function(newsnapshot){
                                document.getElementById(childSnapshot2.key).innerHTML = ( otheruserid == childdata2.user_id ) ?
                                str_before_photo + newsnapshot.val().photoURL + str_before_username + newsnapshot.val().name + c :
                                str_before_photo + user_photoURL + str_before_username + user_name + c;
                            });
                        });
                        postsRef.on('child_added',function(data){
                            second_count+=1;
                            if(second_count>first_count){
                                var childdata = data.val();
                                var a = str_before_id + data.key + str_after_id + str_before_photo;
                                var b = str_before_content + childdata.text.split('\n').join('<br>') + str_before_time 
                                        + totime(childdata.createdAt,8)  + str_after_content;
                                var c = str_before_content + childdata.text.split('\n').join('<br>') + str_before_time 
                                + totime(childdata.createdAt,8)  + "</small></p></div></div>";
                                otherdata.once('value').then(function (othersnapshot) {
                                    total_post[total_post.length] = ( otheruserid == childdata.user_id ) ? 
                                    a + othersnapshot.val().photoURL + str_before_username + othersnapshot.val().name + b :
                                    a + user_photoURL  + str_before_username + user_name + b ;
                                    document.getElementById('post_list').innerHTML = total_post.join(''); 
                                    
                                    otherdata.on('value',function(newsnapshot){
                                        document.getElementById(data.key).innerHTML = ( otheruserid == childdata.user_id ) ?
                                        str_before_photo + newsnapshot.val().photoURL + str_before_username + newsnapshot.val().name + c :
                                        str_before_photo + user_photoURL + str_before_username + user_name + c;
                                    });
                                });
                            }
                        })
                    }
                });
            });
            if( zero_count == 0 ){
                postsRef.on('child_added',function(data){
                    var childdata = data.val();
                    var a = str_before_id + data.key + str_after_id + str_before_photo;
                    var b = str_before_content + childdata.text.split('\n').join('<br>') + str_before_time 
                            + totime(childdata.createdAt,8)  + str_after_content;
                    var c = str_before_content + childdata.text.split('\n').join('<br>') + str_before_time 
                    + totime(childdata.createdAt,8)  + "</small></p></div></div>";
                    otherdata.once('value').then(function (othersnapshot) {
                        total_post[total_post.length] = ( otheruserid == childdata.user_id ) ? 
                        a + othersnapshot.val().photoURL + str_before_username + othersnapshot.val().name + b :
                        a + user_photoURL  + str_before_username + user_name + b ;
                        document.getElementById('post_list').innerHTML = total_post.join(''); 
                        
                        otherdata.on('value',function(newsnapshot){
                            document.getElementById(data.key).innerHTML = ( otheruserid == childdata.user_id ) ?
                            str_before_photo + newsnapshot.val().photoURL + str_before_username + newsnapshot.val().name + c :
                            str_before_photo + user_photoURL + str_before_username + user_name + c;
                        });
                    });
                })
            }
        })
        .catch(e => console.log(e.message));
}
//寄送訊息
function sendmessage(){
    var post_txt = document.getElementById('comment');
    if (post_txt.value != "") {
        post_txt_value = post_txt.value;//同一個值同時給newMessageRef和newMessageRef2的話 後面那個會沒拿到值，所以多另一個
        currenttime = firebase.database.ServerValue.TIMESTAMP;
        var newMessageRef = firebase.database().ref("message/"+loginUserid+"/"+otheruserid).push();
        var newMessageRef2 = firebase.database().ref("message/"+otheruserid+"/"+loginUserid).push();
        newMessageRef.set({
            'user_id': loginUserid,
            'text': post_txt.value,
            'createdAt': currenttime
        }).then(function(e){
            if(otheruserid!=loginUserid) newMessageRef2.set({
                'user_id': loginUserid,
                'text': post_txt_value,
                'createdAt': currenttime
            })
        }).catch(function(e){
            alert("Error,您並未送出訊息");
            window.location.href="index.html";
        });
        post_txt.value="";
    }
}

function setfriend(){
    firebase.database().ref('userlist/').once('value').then(function (snapshot) {
        snapshot.forEach(function(childSnapshot) {
            var childdata = childSnapshot.key;
            if( loginUserid != childdata ){
                firebase.database().ref('users/' + loginUserid + '/touser/' + childdata)
                .set({ 'isfriend': true })
            }
        });
    })
}
//時間處理
function padLeft(str,leng){
    if(str.length >= leng) return str;
    else return padLeft("0"+str,leng);
}

var monthday = [ 0 , 31 , 28 , 31 , 30 , 31 , 30 , 31 , 31 , 30 , 31 , 30 , 31 ]
function tomonthday(days){
    var i;
    days+=1;
    for(i=1;i<=12;i++) {
        if(days<=monthday[i]) break;
        days-=monthday[i];
    }
    i=padLeft(i.toString(),2);
    days=padLeft(days.toString(),2);
    return i+"/"+days;
}

function totime(stamp,area){
    var num = Math.floor(stamp/1000-1514764800)+area*60*60;
    var day = [];
    var time = [];
    day[0]=Math.floor(num/(365*24*60*60))+2018;
    day[1]=Math.floor((num%(365*24*60*60))/(24*60*60)); day[1]=tomonthday(day[1]);
    time[0]=Math.floor((num%(24*60*60))/(60*60));       time[0]=padLeft(time[0].toString(),2);
    time[1]=Math.floor((num%(60*60))/60);               time[1]=padLeft(time[1].toString(),2);
    time[2]=num%60;                                     time[2]=padLeft(time[2].toString(),2);
    return day.join('/')+' '+time.join(':')+" (UTC"+"+"+area+")";
}

//顯示處理
var isopen=false;
window.onresize = function (){
    isopen=!isopen;
    changemain();
}

var idchat,navbarheight,fix;
function changemain(){
    isopen = !isopen;
    idchat = document.getElementById('chat');
    navbarheight = parseInt(getComputedStyle(document.getElementsByClassName('navbar')[0]).height);//取得navbar height的整數部分
    fix = ( document.body.clientHeight>navbarheight )? 17 : 0; //17=卷軸寬度
    if( document.body.clientWidth+fix>767.98 ){
        idchat.className = "contain";
        idchat.className += ( isopen ) ? "smallermain":"biggermain";
        idchat.style.transform = ( isopen ) ? "translateX(25%)translateX(20px)" : "translateX(0px)" ;//20%*(100/80)=25% 20px=padding*2
    }
    else{
        idchat.className = "containbiggermain";
        idchat.style.transform="translateX(0px)";
    }
}

//設定畫面的功能
function changename(){
    var newname = document.getElementById('changename').value;
    firebase.database().ref('users/'+loginUserid+'/public').update({ 'name': newname });
    firebase.database().ref('userlist/'+loginUserid).update({ 'name': newname });
}

function setimg(){
    return  function(event) { 
                document.getElementById('showimg').src = event.target.result;
                firebase.database().ref('users/'+loginUserid+'/public').update({ 'photoURL': event.target.result });
            };
}

function readimg(files) {
    var file = files[0];
    if(file){
        var reader = new FileReader();
        reader.onload = setimg();
        reader.readAsDataURL(file);
    }
}