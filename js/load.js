//計時相關
var time_limit = 15*60;//s

var loadState = { 
    preload: function () {
        // Add a 'loading...' label on the screen 
        var loadingLabel = game.add.text(game.width/2, 90+150, 'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);
        // Display the progress bar 
        var progressBar = game.add.sprite(game.width/2, 90+200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5); 
        game.load.setPreloadSprite(progressBar);
        game.load.audio('audio1', 'img/bgm1.ogg');

        game.load.tilemap('map', 'assets/6.json', null, Phaser.Tilemap.TILED_JSON);
        game.load.image('tile1', 'assets/wall.png');
        game.load.image('tile2', 'assets/chest.png');

        game.load.spritesheet('player', 'assets/player2.png', 48, 48);
        game.load.spritesheet('chest', 'assets/chest.png', 64, 64);//tile大小同
        game.load.spritesheet('explosion','assets/explosion.png',96,96);
        game.load.spritesheet('ice','assets/ice.png',192,192);
        game.load.spritesheet('guard','assets/guard.png',50,48);
        game.load.image('nail1','assets/nail1.png');
        game.load.image('nail2','assets/nail2.png');
        game.load.image('hole','assets/hole.png');
        game.load.image('key1','assets/key1.png');        
        game.load.image('key2','assets/key2.png');

        game.load.image('box', 'assets/box.png');
        game.load.image('machine', 'assets/machine.png');
        game.load.image('battery', 'assets/battery.png');
        game.load.image('tool1', 'assets/tool1.png');
        game.load.image('banana', 'assets/banana.png');
        game.load.spritesheet('boom', 'assets/boom.png',65,65);
        game.load.spritesheet('electricV', 'assets/electricV.png',25,2152);  // 6.15 -------------垂直方向电流 
        game.load.spritesheet('electricH', 'assets/electricH.png',2152,25);   //6.15 -------------水平方向电流
    }, 
    create: function() { 
        this.ref = firebase.database().ref(game.global.room_name);
        this.inf_ref = firebase.database().ref("game_information/" + game.global.room_name.split('/')[1]);
        this.ref.once("value").then(function(snapshot){
            var total_ready = 0;
            var j = 0;
            snapshot.forEach(function(childSnapshot) {
                var cdata = childSnapshot.child("public").val();
                var ready = cdata.ready
                var my_number = cdata.my_number;
                total_ready += ready;
                var player = {
                    name: childSnapshot.key,
                    x: cdata.x,
                    y: cdata.y,
                    boom: cdata.boom,
                    battery: cdata.battery,
                    tool1: cdata.tool1,
                    banana: cdata.banana,
                    key1: cdata.key1,
                    key2: cdata.key2
                };
                game.global.player[my_number] = player;
                if( childSnapshot.key == user_name ) game.global.my_number = my_number;
                j++;
            });
            for(;j<4;j++){
                game.global.player[j] = "";
            }
            if( total_ready == snapshot.numChildren() && total_ready) {
                if( game.global.my_number != 5 ) {

                    game.global.number_of_player = snapshot.numChildren();

                    loadState.inf_ref.once("value",function(snapshot){
                        var data = snapshot.val();
                        var currenttime = Date.now();
                        if( time_limit < ( currenttime - data.start_time )/1000 ){
                            console.log(data.start_time+" "+currenttime)
                            loadState.inf_ref.remove();
                            loadState.ref.remove();
                            loadState.create();
                        }
                        else{
                            game.global.chest_events = data.chest_events;
                            game.global.object_pos = data.object_pos;
                            game.global.true_key = data.true_key;
                            game.global.start_time = data.start_time;
                            game.global.hints = data.hints;
                            game.state.start('play');
                        }
                    });

                }
                else {
                    alert("this game is playing.");
                    changeto_gamemode();
                }
            }
            else if( snapshot.numChildren()<4 || game.global.my_number != 5 ) game.state.start('waiting');
            else {
                alert("The number of people has reached the limit.");
                changeto_gamemode();
            }
        });
    }
};